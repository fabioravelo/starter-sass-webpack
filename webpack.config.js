// Décommenter le code plus bas pour activer la minification

var path = require("path");
var webpack = require("webpack");


module.exports = {
  entry: ['./src/js/main.js'],
  output: {
    filename: 'assets/js/bundle.js'
  },
  module: {
	 rules: [],
	 loaders: [
    {
			test: /\.js$/,
			loader: 'source-map',
		}
    ],
  },
  plugins: [
	  new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
	/*new webpack.optimize.UglifyJsPlugin({minimize: true}),*/
  ],
  devtool: 'source-map'
};
